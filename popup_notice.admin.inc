<?php

/**
 * @file
 * Admin pages for the Popup Notice module.
 */

/**
 * Menu callback.
 */
function popup_notice_admin($form, &$form_state) {
  $text = variable_get('popup_notice_text', '');
  if (isset($text['value']) && isset($text['format'])) {
    $text['value'] = check_markup($text['value'], $text['format']);
  }
  $popup_modules = popup_notice_popup_modules();
  $form['popup_notice_text'] = array(
    '#type' => 'text_format',
    '#title' => t('Text to display'),
    '#description' => t('Text to be displayed in the popup at page load'),
    '#default_value' => (isset($text['value'])) ? $text['value'] : '',
    '#format' => (isset($text['format'])) ? $text['format'] : '',
  );
  $form['popup_notice_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Popup width'),
    '#description' => t('Popup width in pixels'),
    '#default_value' => variable_get('popup_notice_width', 400),
    '#required' => TRUE,
  );
  $form['popup_notice_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Popup height'),
    '#description' => t('Popup height in pixels'),
    '#default_value' => variable_get('popup_notice_height', 300),
    '#required' => TRUE,
  );
  $form['popup_notice_popup_module'] = array(
    '#type' => 'radios',
    '#title' => t('Popup module to use'),
    '#description' => t('The popup module expects Colorbox or Lightbox2 to be installed'),
    '#options' => array('colorbox' => 'Colorbox', 'lightbox2' => 'Lightbox2'),
    '#default_value' => variable_get('popup_notice_popup_module', array_search(TRUE, $popup_modules)),
  );
  foreach ($popup_modules as $module => $status) {
    if (!$status) {
      $form['popup_notice_popup_module'][$module] = array('#disabled' => TRUE);
    }
  }
  $form['popup_notice_interval'] = array(
    '#type' => 'textfield',
    '#title' => t('Interval'),
    '#description' => t('Period during which the popup will not be presented again to the visitor (in seconds)'),
    '#default_value' => variable_get('popup_notice_interval', 86400),
    '#required' => TRUE,
  );
  $form['popup_notice_restrict_by_path'] = array(
    '#type' => 'radios',
    '#title' => t('Show popup on specific pages'),
    '#options' => array(
      0 => t('All pages except those listed'),
      1 => t('Only the listed pages'),
    ),
    '#default_value' => (int) variable_get('popup_notice_restrict_by_path', 0),
  );
  $form['popup_notice_paths_list'] = array(
    '#type' => 'textarea',
    '#description' => t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", array(
      '%blog' => 'blog',
      '%blog-wildcard' => 'blog/*',
      '%front' => '<front>',
    )),
    '#default_value' => variable_get('popup_notice_paths_list', ''),
  );
  return system_settings_form($form);
}

/**
 * Validation function for the admin form.
 */
function popup_notice_admin_validate($form, &$form_state) {
  if (!is_numeric($form_state['values']['popup_notice_interval'])) {
    form_set_error('popup_notice_interval', t('The interval has to be a number.'));
  }
  if (!is_numeric($form_state['values']['popup_notice_width'])) {
    form_set_error('popup_notice_width', t('The popup width has to be a number.'));
  }
  if (!is_numeric($form_state['values']['popup_notice_height'])) {
    form_set_error('popup_notice_height', t('The popup height has to be a number.'));
  }
}
