SUMMARY
=======

This module allows to display a popup on specified pages the first time a user
visits your site, and then again after a configurable period of time. It uses
local storage instead of cookies, which are sent to the server every time a page
is loaded. The popup is managed in JavaScript by the client, so that it does not
interfere with page caching.

This module was written by FMB [1] and sponsored by Pycna [2]. FMB can be found
on IRC (Freenode, #drupal-fr or #drupal.cat).


REQUIREMENTS
============

A popup module is required. Currently, Lightbox2 and Colorbox are supported.


INSTALLATION
============

Install as usual, see http://drupal.org/node/895232 for further information.


CONFIGURATION
=============

* Configure user permissions in Administration » People » Permissions.
* The module can be configured at Administration » Configuration » User
  interface » Popup Notice.


OTHER MODULES
=============

Popup Announcement and Popup On Load have similar goals, but unfortunately do
not work too well when page caching is enabled, which was the very reason why
this module was written in the first place.


[1] https://www.drupal.org/u/fmb
[2] http://pycna.com/
