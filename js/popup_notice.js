(function ($) {
  // Display popup at page load.
  $(document).ready(function() {
    if (typeof Drupal.settings.popup_notice != 'undefined' && typeof localStorage != 'undefined') {
      var interval = Drupal.settings.popup_notice.interval;
      var module = Drupal.settings.popup_notice.module;
      var width = Drupal.settings.popup_notice.width;
      var height = Drupal.settings.popup_notice.height;
      var url = Drupal.settings.popup_notice.url;
      var last_displayed = localStorage.getItem('popup_notice_last_displayed');
      var now = $.now();
      if (last_displayed === null || now - last_displayed >= interval) {
        switch (module) {
          case 'lightbox2':
            $('#popup-notice-link').click();
            break;

          case 'colorbox':
            $.colorbox({iframe: false, open: true, href: url, height: height, width: width});
            break;
        }
        localStorage.setItem('popup_notice_last_displayed', now);
      }
    }
  });
})(jQuery);
